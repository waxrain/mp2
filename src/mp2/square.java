/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mp2;

/**
 *
 * @author asus
 */
public class square {
    public int x; // leftmost == minX
    public int y; // topmost == maxY
    public int width;
    public int height;
    
    public int maxX(){
        return x + width;
    }
    public int maxY(){
        return y + height;
    }
    
    public square(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
            
    public int xn (int n){
        switch (n){
            case 1:
                return x;
            case 2:
                return x + this.width;
        }
        return -9999;
    }
    public int yn (int n){
        switch (n){
            case 1:
                return y;
            case 2:
                return y + this.height;
        }
        return -9999;
    }
    
    public float centerGrad(square S){
        float gradient = 0;
        gradient = (S.centerY() - this.centerY())/ (S.centerX() - this.centerX());
        return gradient;
    }
    
    /****
     * @param S Gradient range of S to this
     * @return Array of float[2] containing [0] as minimal gradient and [1] as max gradient
     ***/
    public float[] minmaxGrad(square S){
        float minmax[] = new float[2]; // [0] = min, [1] = max
        minmax[0] = 10001; minmax[1] = -10001;
        // checks gradient from points of this to S, clockwise starting from top leftmost
        
        for (int i = 1; i <= 2; i++ ){
            for (int j = 1; j <= 2; j++){
                for (int k = 1; k <= 2; k++){
                    for (int l = 1; l <= 2; l++){
                        float temp = gradient(S.yn(i), this.yn(k), S.xn(j), this.yn(l));
                        if (minmax[0] > temp){
                            minmax [0] = temp;
                        }
                        if (minmax[1] < temp){
                            minmax[1] = temp;
                        }
                    }
                }
            }
        }
        return minmax;
    }
    
    public float gradient(float y1, float y2, float x1, float x2){
        return ((y1 - y2) / (x1 - x2));
    }
    
    
    public float centerX(){
        return (x + (width/2));
    }
    public float centerY(){
        return (y + (height/2));
    }
    
    
    /***
     * @param S is the square inside THIS
     * @return true if S is inside THIS
     **/
    public boolean isInside(square S){ 
        // check min-max
        return ((S.x > this.x) && 
                (S.y > this.y) && 
                (S.maxX() < this.maxX()) &&
                (S.maxY() < this.maxY()));
    }
    
}
