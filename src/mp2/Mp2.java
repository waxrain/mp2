/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mp2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author asus
 */
public class Mp2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nSquares = in.nextInt();
        ArrayList<square> squareList = new ArrayList<>();
        int maxRangeX = 10000, maxRangeY = 10000;
        
        for (int i = 0; i < nSquares; i++){
            int x, y, width, height;
            x = in.nextInt();
            y = in.nextInt();
            width = in.nextInt();
            height = in.nextInt();
            squareList.add(new square(x, y, width, height));
        }
        int maxSquareHit = 0;
        // brute force by frame limit, clockwise from left-side
        for (int rightSide = 0; rightSide <= maxRangeY; rightSide++){
            for (int XSide = 1; XSide <= maxRangeX; XSide++){ // both top and bottom frame
                // test each line passes through square, 
                // y = mx + c
                int topSquareCounter = 0;
                int bottomSquareCounter = 0;
                for (square S : squareList){
                    // top side; y = 0
                    float y1 = ((float)rightSide / (float)(-1 * XSide)) * S.x + rightSide;
                    float y2 = ((float)rightSide / (float)(-1 * XSide)) * S.maxX() + rightSide;
                    
                    // check point overlap
                    // if max - min < w1 + w2 -> overlap
                    float min = Float.min(y1, y2);
                    float max = Float.max(y1, y2);
                    
                    min = Float.min(min, S.y);
                    max = Float.max(max, S.maxY());
                    
                    if ((max - min) - (S.height + Math.abs(y1 - y2)) < 0.001 ){
                        // overlaps 
                        topSquareCounter++;
                    }
                    
                    // bottom side; y = 10000
                    float yb1 = ((float) (rightSide - maxRangeY) / (float) (-1 * XSide)) * S.x + rightSide;
                    float yb2 = ((float) (rightSide - maxRangeY) / (float) (-1 * XSide)) * S.maxX() + rightSide;
                    
                    float minBottom = Float.min(yb1, yb2);
                    float maxBottom = Float.max(yb1, yb2);
                    
                    minBottom = Float.min(minBottom, S.y);
                    maxBottom = Float.max(maxBottom, S.maxY());
                    if ((maxBottom - minBottom) - (S.height + Math.abs(yb1 - yb2)) < 0.001 ){
                        // overlaps 
                        bottomSquareCounter++;
                    }
                }
                if (maxSquareHit < topSquareCounter){
                    maxSquareHit = topSquareCounter;
                }
                if (maxSquareHit < bottomSquareCounter){
                    maxSquareHit = bottomSquareCounter;
                }
            }
            
            // process right-side to left-side lines
            for (int YSide = 0; YSide <= maxRangeY; YSide++){
                int squareCounter = 0;
                for (square S : squareList){                    
                    float y1 = ((float)rightSide - YSide / ((float)maxRangeY)) * S.x + rightSide;
                    float y2 = ((float)rightSide - YSide / ((float)maxRangeY)) * S.maxX() + rightSide;
                    
                    float min = Float.min(y1, y2);
                    float max = Float.max(y1, y2);
                    
                    min = Float.min(min, S.y);
                    max = Float.max(max, S.maxY());
                    if ((max - min) - (S.height + Math.abs(y1 - y2)) < 0.001 ){
                        // overlaps 
                        squareCounter++;
                    }
                }
                if (maxSquareHit < squareCounter){
                    maxSquareHit = squareCounter;
                }
            }
            
            // process left-side to top & bottom 
            // separated in case maxX and maxY has diff. values
            for (int XSide = 1; XSide <= maxRangeX; XSide++){ // both top and bottom frame
                // test each line passes through square, 
                // y = mx + c
                int topSquareCounter = 0;
                int bottomSquareCounter = 0;
                for (square S : squareList){
                    // top side; y = 0
                    float y1 = ((float)rightSide / (float)(maxRangeX - XSide)) * S.x + rightSide;
                    float y2 = ((float)rightSide / (float)(maxRangeX - XSide)) * S.maxX() + rightSide;
                    
                    // check point overlap
                    // if max - min < w1 + w2 -> overlap
                    float min = Float.min(y1, y2);
                    float max = Float.max(y1, y2);
                    
                    min = Float.min(min, S.y);
                    max = Float.max(max, S.maxY());
                    
                    if ((max - min) - (S.height + Math.abs(y1 - y2)) < 0.001 ){
                        // overlaps 
                        topSquareCounter++;
                    }
                    
                    // bottom side; y = 10000
                    float yb1 = ((float) (rightSide - maxRangeY) / (float) (maxRangeX - XSide)) * S.x + rightSide;
                    float yb2 = ((float) (rightSide - maxRangeY) / (float) (maxRangeX - XSide)) * S.maxX() + rightSide;
                    
                    float minBottom = Float.min(yb1, yb2);
                    float maxBottom = Float.max(yb1, yb2);
                    
                    minBottom = Float.min(minBottom, S.y);
                    maxBottom = Float.max(maxBottom, S.maxY());
                    if ((maxBottom - minBottom) - (S.height + Math.abs(yb1 - yb2)) < 0.001 ){
                        // overlaps 
                        bottomSquareCounter++;
                    }
                }
                if (maxSquareHit < topSquareCounter){
                    maxSquareHit = topSquareCounter;
                }
                if (maxSquareHit < bottomSquareCounter){
                    maxSquareHit = bottomSquareCounter;
                }
            }
//            System.out.println(rightSide);
        }
        // process top to bottom
        for (int topSide = 0; topSide <= maxRangeX; topSide++){
            for (int botSide = 0; botSide <= maxRangeX; botSide++){
                int squareCounter = 0;
                if (topSide != botSide){
                    // gradient is not NaN
                    for (square S : squareList){
                        float y1 =  ((float) (0 - 10000) / (float)(topSide - botSide)) * S.x;
                        float y2 =  ((float) (0 - 10000) / (float)(topSide - botSide)) * S.maxX();
                        float min = Float.min(y1, y2);
                        float max = Float.max(y1, y2);

                        min = Float.min(min, S.y);
                        max = Float.max(max, S.maxY());

                        if ((max - min) - (S.height + Math.abs(y1 - y2)) < 0.001 ){
                            // overlaps 
                            squareCounter++;
                        }
                    }
                    
                } else { // gradient results in NaN, check w/ straight vertical line with x == topSide == botSide
                    for (square S : squareList){
                        if (S.x <= topSide && (S.x + S.width) >= topSide){
                            squareCounter++;
                        }
                    }
                }
            }
        }
        
        System.out.println("Res: " + maxSquareHit);
        
    }
    
    
    public float[][][] generateGradientMatrix(ArrayList<square> Squares){
        float[][][] gradMatrix;
        gradMatrix = new float[2][Squares.size()][];
        for (int i = 0; i < 2; i++){
            gradMatrix[i] = new float[Squares.size()][];
            for (int j = 0; j < Squares.size(); j++){
                gradMatrix[i][j] = new float[Squares.size()];
            }
        }
        
        return gradMatrix;
    }
    
}
